//
//  RegActionDate.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "RegActionDate.h"
#import "Application.h"


@implementation RegActionDate

@dynamic actionType;
@dynamic inDocTypeSeqNo;
@dynamic duplicateCounter;
@dynamic actionDate;
@dynamic docType;
@dynamic applNo;

@end
