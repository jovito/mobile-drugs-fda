//
//  ProductDetailsViewController.h
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/21/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "ChemicalType_Lookup.h"
#import "OpenEars.h"
#import "Product.h"

@interface DrugDetailsViewController : UIViewController<UITableViewDataSource, UITabBarDelegate>

@property(strong,nonatomic) IBOutlet UITableView *tblDrug;

@property(strong,nonatomic) Product *product;
@property(strong,nonatomic) OpenEars *openEars;

@end
