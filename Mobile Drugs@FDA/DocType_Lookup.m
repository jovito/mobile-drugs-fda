//
//  DocType_Lookup.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "DocType_Lookup.h"


@implementation DocType_Lookup

@dynamic docType;
@dynamic docTypeDesc;

@end
