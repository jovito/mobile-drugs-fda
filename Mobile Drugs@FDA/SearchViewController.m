//
//  FirstViewController.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/20/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@property(strong,nonatomic) NSArray *letters;
@property(strong,nonatomic) NSMutableDictionary *content;
@property(strong,nonatomic) NSArray *keys;

@end

@implementation SearchViewController

@synthesize letters;
@synthesize content;
@synthesize keys;

@synthesize tblDrugs;
@synthesize sbSearchBar;
@synthesize dataLoader;
@synthesize openEars;
@synthesize products;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view, typically from a nib.
    letters = [NSArray arrayWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H",
               @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U",
               @"V", @"W", @"X", @"Y", @"Z", nil];
    content = [[NSMutableDictionary alloc] init];
    
    dataLoader = [[DataLoader alloc] init];
    openEars = [[OpenEars alloc] init];
    openEars.delegate = self;

    [sbSearchBar setImage:[UIImage imageNamed:@"mic"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
    [sbSearchBar setPositionAdjustment:UIOffsetMake(-10, 0) forSearchBarIcon:UISearchBarIconBookmark];
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
    [sbSearchBar resignFirstResponder];
    sbSearchBar.text = @"";
    products = [[NSArray alloc] init];
    [self createSections];
    [tblDrugs reloadData];
    
//    [openEars createDictionary];
    [openEars startListening];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) createSections
{
    [content removeAllObjects];
    
    for (NSString *letter in letters)
    {
        NSMutableArray *values = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dict in products)
        {
            NSString *name = [dict objectForKey:@"Drug Name"];
            
            if ([name hasPrefix:letter])
            {
                if (![values containsObject:dict])
                {
                    [values addObject:dict];
                }
            }
        }
        
        if (values.count > 0)
        {
            [content setValue:values forKey:letter];
        }
    }
    
    keys =  [[content allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
}

#pragma mark - UITableViewDataSource
- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return keys;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return [keys indexOfObject:title];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [keys count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *prefix = [keys objectAtIndex:section];
    NSArray *list = [content objectForKey:prefix];
    return list.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    UILabel *label = [[UILabel alloc] init];
    
    label.text = [keys objectAtIndex:section];
    label.backgroundColor = [UIColor lightGrayColor];
    label.textColor = [UIColor lightTextColor];
    label.userInteractionEnabled = YES;
    [label setTag:section+1];
    return label;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [keys objectAtIndex:section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SearchResultsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *prefix = [keys objectAtIndex:indexPath.section];
    NSArray *arr = [content objectForKey:prefix];
    NSDictionary *dict = [arr objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [dict objectForKey:@"Drug Name"];
    cell.detailTextLabel.text = [dict objectForKey:@"Active Ingredient"];
    
    return cell;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showSubResults"])
    {
        NSIndexPath *indexPath = [tblDrugs indexPathForSelectedRow];
        NSString *prefix = [keys objectAtIndex:indexPath.section];
        NSArray *arr = [content objectForKey:prefix];
        DrugsSubResultsViewController *destViewController = segue.destinationViewController;
        destViewController.product = [arr objectAtIndex:indexPath.row];
    }
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)bar
{
    products = [dataLoader search:bar.text];
    
    [self createSections];
    [tblDrugs reloadData];
    [bar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)bar
{
    products = [dataLoader search:bar.text];
    
    [self createSections];
    [tblDrugs reloadData];
}

#pragma mark - OpenEarsDelegate
-(void) listeningStarted
{
    sbSearchBar.placeholder = @"Speak now";
}

-(void) listeningEnded:(NSString*)capturedText
{
    sbSearchBar.text = capturedText;
    
    products = [dataLoader search:capturedText];
    
    [self createSections];
    [tblDrugs reloadData];
}

@end
