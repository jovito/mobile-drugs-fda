//
//  AppDoc.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "AppDoc.h"
#import "Application.h"


@implementation AppDoc

@dynamic appDocID;
@dynamic seqNo;
@dynamic docType;
@dynamic docTitle;
@dynamic docURL;
@dynamic docDate;
@dynamic actionType;
@dynamic duplicateCounter;
@dynamic applNo;

@end
