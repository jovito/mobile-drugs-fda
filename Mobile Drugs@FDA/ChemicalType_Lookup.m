//
//  ChemicalType_Lookup.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "ChemicalType_Lookup.h"


@implementation ChemicalType_Lookup

@dynamic chemicalTypeID;
@dynamic chemicalTypeCode;
@dynamic chemicalTypeDescription;

@end
