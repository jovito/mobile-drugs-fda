//
//  Product_TECode.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "Product_TECode.h"
#import "Application.h"
#import "Product.h"


@implementation Product_TECode

@dynamic teCode;
@dynamic teSequence;
@dynamic productMktStatus;
@dynamic applNo;
@dynamic productNo;

@end
