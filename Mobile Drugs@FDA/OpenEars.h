//
//  OpenEars.h
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/21/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DataLoader.h"
#import "Util.h"
#import <Slt/Slt.h>
#import <OpenEars/AcousticModel.h>
#import <OpenEars/FliteController.h>
#import <OpenEars/LanguageModelGenerator.h>
#import <OpenEars/OpenEarsEventsObserver.h>
#import <OpenEars/PocketsphinxController.h>

@protocol OpenEarsDelegate;

@interface OpenEars : NSObject<OpenEarsEventsObserverDelegate>
{
    FliteController *fliteController;
    Slt *slt;
    OpenEarsEventsObserver *openEarsEventsObserver;
    PocketsphinxController *pocketsphinxController;
}

@property(strong, nonatomic) FliteController *fliteController;
@property(strong, nonatomic) Slt *slt;
@property (strong, nonatomic) OpenEarsEventsObserver *openEarsEventsObserver;
@property (strong, nonatomic) PocketsphinxController *pocketsphinxController;
@property (strong, nonatomic) id<OpenEarsDelegate> delegate;

-(void) createDictionary;
-(void) speak:(NSString*)text;
-(void) startListening;
-(void) stopListening;
@end

@protocol OpenEarsDelegate
-(void) listeningStarted;
-(void) listeningEnded:(NSString*)capturedText;
@end

