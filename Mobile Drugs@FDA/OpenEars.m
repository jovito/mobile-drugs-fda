//
//  OpenEars.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/21/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "OpenEars.h"
#import "AppDelegate.h"

@interface OpenEars()
{
    NSMutableString *_capturedText;
}

@end

@implementation OpenEars

@synthesize fliteController;
@synthesize slt;
@synthesize openEarsEventsObserver;
@synthesize pocketsphinxController;
@synthesize delegate;

- (FliteController *)fliteController
{
	if (fliteController == nil)
    {
		fliteController = [[FliteController alloc] init];
	}
	return fliteController;
}

- (Slt *)slt
{
	if (slt == nil)
    {
		slt = [[Slt alloc] init];
	}
	return slt;
}

- (OpenEarsEventsObserver *)openEarsEventsObserver
{
	if (openEarsEventsObserver == nil)
    {
		openEarsEventsObserver = [[OpenEarsEventsObserver alloc] init];
	}
	return openEarsEventsObserver;
}

- (PocketsphinxController *)pocketsphinxController
{
	if (pocketsphinxController == nil)
    {
		pocketsphinxController = [[PocketsphinxController alloc] init];
	}
	return pocketsphinxController;
}

-(void) createDictionary
{
    LanguageModelGenerator *lmGenerator = [[LanguageModelGenerator alloc] init];
    DataLoader *dataLoader = [[DataLoader alloc] init];
    
    NSArray *products = [dataLoader searchAllDrugs];
    NSMutableArray *cappedWords = [[NSMutableArray alloc] initWithCapacity:products.count];
    int max = 0;
    
    for (Product *p in products)
    {
        // why are we limited to 580 entries??
        if (max == 580)
        {
            break;
        }
        
        if (!p.drugName || p.drugName.length <=0 ||
            [Util string:p.drugName containsString:@"\""] ||
            [Util string:p.drugName containsString:@"#"] ||
            [Util string:p.drugName containsString:@"-"] ||
            [Util string:p.drugName containsString:@"%"] ||
            [Util string:p.drugName containsString:@"\\"] ||
            [Util string:p.drugName containsString:@"/"] ||
            [Util string:p.drugName containsString:@"."] ||
            [Util string:p.drugName containsString:@","] ||
            [Util string:p.drugName containsString:@"~"] ||
            [Util string:p.drugName containsString:@";"] ||
            [Util string:p.drugName containsString:@"("] ||
            [Util string:p.drugName containsString:@")"] ||
            [Util string:p.drugName containsString:@" "] ||
            [Util string:p.drugName containsString:@"0"] ||
            [Util string:p.drugName containsString:@"1"] ||
            [Util string:p.drugName containsString:@"2"] ||
            [Util string:p.drugName containsString:@"3"] ||
            [Util string:p.drugName containsString:@"4"] ||
            [Util string:p.drugName containsString:@"5"] ||
            [Util string:p.drugName containsString:@"6"] ||
            [Util string:p.drugName containsString:@"7"] ||
            [Util string:p.drugName containsString:@"8"] ||
            [Util string:p.drugName containsString:@"9"])
        {
            NSLog(@"%@", p.drugName);
            continue;
        }
        NSString *cap = [p.drugName uppercaseString];
        
        if (![cappedWords containsObject:cap])
        {
            [cappedWords addObject:cap];
            max++;
        }
    }
    
    NSString *name = @"drugs";
    NSError *err = [lmGenerator generateLanguageModelFromArray:cappedWords
                                                withFilesNamed:name
                                        forAcousticModelAtPath:[AcousticModel pathToModel:@"AcousticModelEnglish"]];
    
    NSDictionary *languageGeneratorResults = nil;
    NSString *lmPath = nil;
    NSString *dicPath = nil;
	
    if([err code] == noErr)
    {
        
        languageGeneratorResults = [err userInfo];
		
        lmPath = [languageGeneratorResults objectForKey:@"LMPath"];
        dicPath = [languageGeneratorResults objectForKey:@"DictionaryPath"];
		NSLog(@"lmPath=%@", lmPath);
        NSLog(@"dicPath=%@", dicPath);
    }
    else
    {
        NSLog(@"Error: %@",[err localizedDescription]);
    }
}

-(void) speak:(NSString*)text
{
    self.openEarsEventsObserver.delegate = self;
    [self.fliteController say:text withVoice:self.slt];
}

-(void) startListening
{
    NSURL *lmURL = [[APP_DELEGATE applicationDocumentsDirectory] URLByAppendingPathComponent:@"drugs.DMP"];
    NSURL *dicURL = [[APP_DELEGATE applicationDocumentsDirectory] URLByAppendingPathComponent:@"drugs.dic"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[lmURL path]])
    {
        NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"drugs" ofType:@"DMP"]];
        
        NSError* err = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:lmURL error:&err])
        {
            NSLog(@"Oops, could copy %@", lmURL);
        }
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[dicURL path]])
    {
        NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"drugs" ofType:@"dic"]];
        
        NSError* err = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:dicURL error:&err])
        {
            NSLog(@"Oops, could copy %@", dicURL);
        }
    }
    
    self.openEarsEventsObserver.delegate = self;
    [self.pocketsphinxController startListeningWithLanguageModelAtPath:[lmURL path]
                                                      dictionaryAtPath:[dicURL path]
                                                   acousticModelAtPath:[AcousticModel pathToModel:@"AcousticModelEnglish"]
                                                   languageModelIsJSGF:NO];
}

-(void) stopListening
{
    [self.pocketsphinxController stopListening];
}

#pragma mark - OpenEarsEventsObserverDelegate
- (void) pocketsphinxDidReceiveHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore utteranceID:(NSString *)utteranceID
{
    if (!_capturedText)
    {
        _capturedText = [[NSMutableString alloc] init];
    }
    
	NSLog(@"The received hypothesis is %@ with a score of %@ and an ID of %@", hypothesis, recognitionScore, utteranceID);
    [_capturedText appendFormat:@"%@%@", _capturedText.length>0 ? @" " : @"", hypothesis];
    [self stopListening];
}

- (void) pocketsphinxDidStartCalibration
{
	NSLog(@"Pocketsphinx calibration has started.");
}

- (void) pocketsphinxDidCompleteCalibration
{
	NSLog(@"Pocketsphinx calibration is complete.");
}

- (void) pocketsphinxDidStartListening
{
	NSLog(@"Pocketsphinx is now listening.");
    if (delegate)
    {
        [delegate listeningStarted];
    }
}

- (void) pocketsphinxDidDetectSpeech
{
	NSLog(@"Pocketsphinx has detected speech.");
}

- (void) pocketsphinxDidDetectFinishedSpeech
{
	NSLog(@"Pocketsphinx has detected a period of silence, concluding an utterance.");
    if (delegate)
    {
        [delegate listeningEnded:_capturedText];
        _capturedText = nil;
    }
}

- (void) pocketsphinxDidStopListening
{
	NSLog(@"Pocketsphinx has stopped listening.");
    if (delegate)
    {
        [delegate listeningEnded:_capturedText];
        _capturedText = nil;
    }
}

- (void) pocketsphinxDidSuspendRecognition
{
	NSLog(@"Pocketsphinx has suspended recognition.");
}

- (void) pocketsphinxDidResumeRecognition
{
	NSLog(@"Pocketsphinx has resumed recognition.");
}

- (void) pocketsphinxDidChangeLanguageModelToFile:(NSString *)newLanguageModelPathAsString andDictionary:(NSString *)newDictionaryPathAsString
{
	NSLog(@"Pocketsphinx is now using the following language model: \n%@ and the following dictionary: %@",newLanguageModelPathAsString,newDictionaryPathAsString);
}

- (void) pocketSphinxContinuousSetupDidFail
{ // This can let you know that something went wrong with the recognition loop startup. Turn on OPENEARSLOGGING to learn why.
	NSLog(@"Setting up the continuous recognition loop has failed for some reason, please turn on OpenEarsLogging to learn more.");
}
- (void) testRecognitionCompleted
{
	NSLog(@"A test file that was submitted for recognition is now complete.");
}

@end
