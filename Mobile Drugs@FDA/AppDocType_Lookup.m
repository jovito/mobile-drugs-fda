//
//  AppDocType_Lookup.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "AppDocType_Lookup.h"
#import "AppDoc.h"


@implementation AppDocType_Lookup

@dynamic sortOrder;
@dynamic appDocType;

@end
