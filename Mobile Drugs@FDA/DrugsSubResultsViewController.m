//
//  DrugsSubResultsViewController.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/24/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "DrugsSubResultsViewController.h"

@interface DrugsSubResultsViewController ()

@end

@implementation DrugsSubResultsViewController

@synthesize product;
@synthesize tblDrugs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark UITableDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *products = [product objectForKey:@"Products"];
    return products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SubResultsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSArray *products = [product objectForKey:@"Products"];
    Product *p = [products objectAtIndex:indexPath.row];
    cell.textLabel.text = p.drugName;
    cell.detailTextLabel.text = p.dosage;
    
    return cell;
}

#pragma mark - Seguer
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDrugDetails"])
    {
        NSIndexPath *indexPath = [tblDrugs indexPathForSelectedRow];
        NSArray *products = [product objectForKey:@"Products"];
        DrugDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.product = [products objectAtIndex:indexPath.row];
    }
}

@end
