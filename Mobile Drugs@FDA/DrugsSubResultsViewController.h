//
//  DrugsSubResultsViewController.h
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/24/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DrugDetailsViewController.h"
#import "Product.h"

@interface DrugsSubResultsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(strong,nonatomic) NSDictionary *product;
@property(strong,nonatomic) IBOutlet UITableView *tblDrugs;

@end
