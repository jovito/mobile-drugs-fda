//
//  FirstViewController.h
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/20/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataLoader.h"
#import "DrugsSubResultsViewController.h"
#import "OpenEars.h"
#import "Product.h"


@interface SearchViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, OpenEarsDelegate>

@property(strong, nonatomic) IBOutlet UITableView *tblDrugs;
@property(strong, nonatomic) IBOutlet UISearchBar *sbSearchBar;

@property(strong, nonatomic) DataLoader *dataLoader;
@property(strong, nonatomic) OpenEars *openEars;
@property(strong, nonatomic) NSArray *products;

-(void) createSections;
@end
