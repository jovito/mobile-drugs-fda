//
//  ReviewClass_Lookup.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/19/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "ReviewClass_Lookup.h"


@implementation ReviewClass_Lookup

@dynamic reviewClassID;
@dynamic reviewCode;
@dynamic longDescription;
@dynamic shortDescription_;

@end
