//
//  ProductDetailsViewController.m
//  Mobile Drugs@FDA
//
//  Created by Jovito Royeca on 11/21/13.
//  Copyright (c) 2013 Jovito Royeca. All rights reserved.
//

#import "DrugDetailsViewController.h"

@interface DrugDetailsViewController ()

@property(strong,nonatomic) NSArray *sections;

@end

@implementation DrugDetailsViewController

@synthesize sections;
@synthesize tblDrug;
@synthesize product;
@synthesize openEars;

- (void) setProduct:(Product *)_product
{
    product = _product;
    sections = [NSArray arrayWithObjects:product.drugName, @"Application", @"Chemical Type", nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view.
    NSString *text = [NSString stringWithFormat:@"Drug Name: %@, Dosage Form/Route: %@, Strength: %@, Active Ingredient: %@",
            product.drugName, product.form, product.dosage, product.activeIngred];
    
    @try
    {
        openEars = [[OpenEars alloc] init];
        [openEars speak:text];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@ : %@", exception, [exception userInfo]);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITABleViewDataSource
//- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    return sections;
//}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return [sections indexOfObject:title];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    switch (section)
    {
        case 0:
        {
            rows = 6;
            break;
        }
        case 1:
        {
            rows = 3;
            break;
        }
        case 2:
        {
            rows = 2;
            break;
        }
    }
    
    return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sections objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSString *section = [self tableView:tblDrug titleForHeaderInSection:indexPath.section];
    Application *app = product.applNo;
    ChemicalType_Lookup *chem = app.chemicalType;
    
    if ([section isEqualToString:product.drugName])
    {
        switch (indexPath.row)
        {
            case 0:
            {
                cell.textLabel.text = @"Dosage Form/Route";
                cell.detailTextLabel.text = product.form;
                break;
            }
            case 1:
            {
                cell.textLabel.text = @"Strength";
                cell.detailTextLabel.text = product.dosage;
                break;
            }
            case 2:
            {
                cell.textLabel.text = @"Active Ingredient";
                cell.detailTextLabel.text = product.activeIngred;
                break;
            }
            case 3:
            {
                cell.textLabel.text = @"Marketing Status";
                cell.detailTextLabel.text = product.productMktStatusString;
                break;
            }
            case 4:
            {
                cell.textLabel.text = @"TE Code";
                cell.detailTextLabel.text = product.teCode;
                break;
            }
            case 5:
            {
                cell.textLabel.text = @"Reference Drug";
                cell.detailTextLabel.text = product.referenceDrugString;
                break;
            }
        }
    }
    
    else if ([section isEqualToString:@"Application"])
    {
        switch (indexPath.row)
        {
            case 0:
            {
                cell.textLabel.text = @"FDA Application No.";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ # %@", app.applTypeString, app.applNo];
                break;
            }
            
            case 1:
            {
                cell.textLabel.text = @"Company";
                cell.detailTextLabel.text = app.sponsorApplicant;
                break;
            }
            case 2:
            {
                cell.textLabel.text = @"Action Type";
                cell.detailTextLabel.text = app.actionType;
                break;
            }
        }
    }
    
    else if ([section isEqualToString:@"Chemical Type"])
    {
        switch (indexPath.row)
        {
            case 0:
            {
                cell.textLabel.text = @"Code";
                cell.detailTextLabel.text = chem.chemicalTypeCode;
                break;
            }
            case 1:
            {
                cell.textLabel.text = @"Description";
                cell.detailTextLabel.text = chem.chemicalTypeDescription;
                break;
            }
        }
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

@end
